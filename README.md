# Dictionarizer

Dictionary generating software.  
Current languages: 

* ger to eng

Requirements:  

* golang
* sqlite //use memory instead (i/o speed is slow locally)


### What does it do?
Say you want to read a book in german but want to recap on some vocabulary before you get into it. 
You give dictionizer a large text file ```book.txt``` and it will generate a dictionary out of the less frequent words.  

### TODO
* Contact Glosbe about API request limits.
* Pdf font for german letters.


### Temp notes
SQLite statement to aggregate word frequencies:
```sql
CREATE TABLE new_table AS 
SELECT orig, transl, COUNT(orig) AS freq 
FROM words 
WHERE LENGTH(orig) > 3 
GROUP BY orig 
ORDER BY freq DESC 
LIMIT 4;
```
