package main

type ApiResult struct {
	Result string `json:"result"`
	Tuc    []struct {
		Phrase struct {
			Text     string `json:"text"`
			Language string `json:"language"`
		} `json:"phrase,omitempty"`
		Meanings []struct {
			Language string `json:"language"`
			Text     string `json:"text"`
		} `json:"meanings,omitempty"`
		MeaningID int64 `json:"meaningId"`
		Authors   []int `json:"authors"`
	} `json:"tuc"`
	Phrase  string `json:"phrase"`
	From    string `json:"from"`
	Dest    string `json:"dest"`
	Authors struct {
		Num6 struct {
			U   string `json:"U"`
			ID  int    `json:"id"`
			N   string `json:"N"`
			URL string `json:"url"`
		} `json:"6"`
		Num76 struct {
			U   string `json:"U"`
			ID  int    `json:"id"`
			N   string `json:"N"`
			URL string `json:"url"`
		} `json:"76"`
		Num2736 struct {
			U   string `json:"U"`
			ID  int    `json:"id"`
			N   string `json:"N"`
			URL string `json:"url"`
		} `json:"2736"`
		Num81326 struct {
			U   string `json:"U"`
			ID  int    `json:"id"`
			N   string `json:"N"`
			URL string `json:"url"`
		} `json:"81326"`
		Num83080 struct {
			U   string `json:"U"`
			ID  int    `json:"id"`
			N   string `json:"N"`
			URL string `json:"url"`
		} `json:"83080"`
		Num86934 struct {
			U   string `json:"U"`
			ID  int    `json:"id"`
			N   string `json:"N"`
			URL string `json:"url"`
		} `json:"86934"`
		Num92056 struct {
			U   string `json:"U"`
			ID  int    `json:"id"`
			N   string `json:"N"`
			URL string `json:"url"`
		} `json:"92056"`
		Num93107 struct {
			U   string `json:"U"`
			ID  int    `json:"id"`
			N   string `json:"N"`
			URL string `json:"url"`
		} `json:"93107"`
	} `json:"authors"`
}
