Project Gutenberg's Also Sprach Zarathustra, by Friedrich Wilhelm Nietzsche

This eBook is for the use of anyone anywhere at no cost and with almost no restrictions whatsoever. You may copy it, give it away or re-use it under the terms of the Project Gutenberg License included with this eBook or online at www.gutenberg.org


Title: Also Sprach Zarathustra

Author: Friedrich Wilhelm Nietzsche

Posting Date: August 5, 2011 [EBook #7205] Release Date: January, 2005 [This file was first posted on March 26, 2003] [Last updated: December 21, 2014]

Language: German


*** START OF THIS PROJECT GUTENBERG EBOOK ALSO SPRACH ZARATHUSTRA ***




Produced by Peter Bellen, derived from HTML files at "Projekt Gutenberg - DE"





Friedrich Nietzsche

Also sprach Zarathustra

Ein Buch für Alle und Keinen




Inhaltsverzeichnis

Erster Theil

Zarathustra's Vorrede

Die Reden Zarathustra's

Von den drei Verwandlungen

Von den Lehrstühlen der Tugend

Von den Hinterweltlern

Von den Verächtern des Leibes

Von den Freuden- und Leidenschaften

Vom bleichen Verbrecher

Vom Lesen und Schreiben

Vom Baum am Berge

Von den Predigern des Todes

Vom Krieg und Kriegsvolke

Vom neuen Götzen

Von den Fliegen des Marktes

Von der Keuschheit

Vom Freunde

Von tausend und Einem Ziele

Von der Nächstenliebe

Vom Wege des Schaffenden

Von alten und jungen Weiblein

Vom Biss der Natter

Von Kind und Ehe

Vom freien Tode

Von der schenkenden Tugend

Zweiter Theil

Das Kind mit dem Spiegel

Auf den glückseligen Inseln

Von den Mitleidigen

Von den Priestern

Von den Tugendhaften

Vom Gesindel

