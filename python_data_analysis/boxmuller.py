#!/usr/bin/python
import matplotlib.pyplot as plt
from random import gauss as gauss
from random import random
import math

def boxmull():
    u1 = random()
    u2 = random()

    r = math.sqrt( -2 * math.log(u1) )
    th = 2 * math.pi * u2

    z1 = r * math.cos(th)
    z2 = r * math.sin(th)

    return (z1, z2)


def gaussian(x, mu, sigma):
    return x*sigma + mu


randarr = []
randarr1 = []
for i in range(10000):
    mu = 5
    sigma = 0.1

    tmp = boxmull()
    randarr += [gaussian(tmp[0], mu, sigma)]

    randarr1 += [gauss(mu,sigma)]


plt.figure(1)
plt.subplot(1,2,1)
plt.hist(randarr, bins=100)
plt.title("Box-Muller transformed random numbers")
plt.subplot(1,2,2)
plt.hist(randarr1, bins=100, color='red')
plt.title("random.gauss() random numbers")
plt.show()
