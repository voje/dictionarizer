#!/usr/bin/python

import sqlite3
import numpy as np
import matplotlib.pyplot as plt

dbname = "also_sprach_zarathustra_full.db"

conn = sqlite3.connect("../tmp_db/"+dbname)

c = conn.cursor()
#conn.commit()
#conn.close

#c.execute("QUERY ??", (args))
#res = c.execute("SELECT * FROM words1 WHERE LENGTH(orig) > 3")  #c points to first result
res = c.execute("SELECT * FROM words1")  #c points to first result

""" possible iteration
for r in c:
    print(r)
"""

freqs = []
r = c.fetchone()
print("row: " + str(r))
freqs += [r[2]]
for r in c:
    freqs += [r[2]]

print(freqs[:10])
print(freqs[-10:])

# Draw word frequency histogram
#plt.ion()
plt.figure(1)
plt.hist(freqs, bins=len(set(freqs)))
#plt.hist(freqs, bins=10)
plt.grid(True)
plt.title('Word frequency distribution')
plt.xlabel('word frequency')
plt.ylabel('number of words')
plt.yscale('log')
plt.show()

conn.close()
