package main 

import (
	"testing"
	"fmt"
)

var filename string = "pdf_print_test.txt"
var output_filename string = "./output_pdf/pdf_print_test.pdf"

type w1_struct struct {
    orig string
    transl string
    freq int
}
var output_samples []w1_struct = []w1_struct{
	w1_struct{orig: "der Apfel", transl: "apple", freq: 42},
	w1_struct{orig: "der Kartoffel", transl: "potato", freq: 42},
	w1_struct{orig: "der Regenschirm", transl: "umbrella", freq: 42},
	w1_struct{orig: "der Bundesstadt", transl: "capital", freq: 42},
	w1_struct{orig: "der Fluss", transl: "river", freq: 42},
	w1_struct{orig: "der Regen", transl: "rain", freq: 42},
	w1_struct{orig: "der Appotheke", transl: "pharmacy", freq: 42},
}

func Test1(*testing.T) { 
	draw_pdf()
}