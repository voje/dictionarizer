package main 

import (
	"github.com/jung-kurt/gofpdf"
	"fmt"
)

var t1 []string = []string{"der Apfel", "die Orange", "die Birne", "das Obst"}
var t2 []string = []string{"apple", "orange", "pear", "fruit"}

func draw_table(pdf *gofpdf.Fpdf) {
	// Column widths
	w := []float64{40.0, 40.0, 30.0, 40.0, 40.0}

	// 	Header - two columnes
    pdf.SetFont("Arial", "B", 13)
	pdf.CellFormat(w[0], 7, "DEU", "1", 0, "C", false, 0, "")
	pdf.CellFormat(w[1], 7, "ENG", "1", 0, "C", false, 0, "")
	pdf.CellFormat(w[2], 7, "", "", 0, "C", false, 0, "")
	pdf.CellFormat(w[0], 7, "DEU", "1", 0, "C", false, 0, "")
	pdf.CellFormat(w[1], 7, "ENG", "1", 1, "C", false, 0, "")
    pdf.SetFont("Arial", "", 11)

    //entries
    var offset int
    var linebreak int
    for i := range output_samples {
		offset = 0
		linebreak = 0
		if i%2 != 0 {
			pdf.CellFormat(w[2], 6, "", "", 0, "C", false, 0, "")
			offset = 3
			linebreak = 1
		}
		pdf.CellFormat(w[0+offset], 6, output_samples[i].orig, "LR", 0, "L", false, 0, "")
		pdf.CellFormat(w[1+offset], 6, output_samples[i].transl, "LR", linebreak, "L", false, 0, "")
	}
	//pdf.CellFormat(wSum, 0, "", "T", 0, "", false, 0, "")
}

func draw_pdf() {
	fmt.Println("Drawing pdf.")
	pdf := gofpdf.New("P", "mm", "A4", "")
	pdf.AddPage()
	pdf.SetFont("Arial", "B", 15)
    pdf.CellFormat(0, 10, "Quick dictionary", "", 1, "C", false, 0, "")
	pdf.SetFont("Arial", "B", 11)
	tmptxt := "Generated from file: " + filename
    pdf.CellFormat(0, 10, tmptxt, "", 1, "C", false, 0, "")
    draw_table(pdf)
	pdf.OutputFileAndClose(output_filename)
	fmt.Println("File saved successfully: ", output_filename)
}









