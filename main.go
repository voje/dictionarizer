package main

import (
    "fmt"
    "os"
    "bufio"
    "io/ioutil"
    "database/sql"
    _"github.com/mattn/go-sqlite3"
    "regexp"
    "net/http"
    "encoding/json"
    "strings"
    _"github.com/jung-kurt/gofpdf"
)

var dbname string = "./tmp_db/dict.db"
var tbname string = "words"
var filename string
var output_filename string
var db *sql.DB = nil
var err error
var client *http.Client = &http.Client{}
var word_count int
var sql_buffer int = 10000 //transaction every n words
var rf_prob map[int]float64
var output_cap = 100        //minimum of translated out of samples
var TRANSL_ERR = "_?_"
var api_call_counter chan int 

//Reduce this weight to increase sampling probability.
var sampling_weight float64 = 1

type w1_struct struct {
    orig string
    transl string
    freq int
}
var samples []w1_struct
var output_samples []w1_struct

func check(e error) {
    if e != nil {
        panic(e.Error())
    }
}

func wordcount(filename string) {
    f,err := os.Open(fmt.Sprintf("./text_files/%s.txt", filename))
    check(err)
    sc := bufio.NewScanner(f)
    sc.Split(bufio.ScanWords)
    wc := 0
    for sc.Scan() {
        wc++
    }
    fmt.Printf("File contains %d words.\n", wc)
    word_count = wc
}

func read_file(filename string) {
    //Empty the table (just in case)
    _,err = db.Exec("DELETE FROM words;")
    check(err);

    wordcount(filename)
    f,err := os.Open(fmt.Sprintf("./text_files/%s.txt", filename))
    check(err)
    rd := bufio.NewReader(f)
    sc := bufio.NewScanner(rd)
    sc.Split(bufio.ScanWords)
    words_scanned := 0

    insert_stmt := "INSERT INTO words (orig) VALUES "
    insert_args := ""

    for sc.Scan() {
        w := sc.Text()
        words_scanned++

        if len(w) == 1 {
          continue
        }

        //replace '"` with _
        w = strings.Replace(w, "'", "_", -1)
        w = strings.Replace(w, "\"", "_", -1)
        w = strings.Replace(w, "`", "_", -1)

        //remove stops, commas, exclamation marks, question marks
        for i:=0; i<3; i++ {
            mtch,err := regexp.MatchString("^[a-zA-Z]+[.,!?:]$", w)
            check(err)
            if mtch {
                w = w[:len(w)-1]
            }
        }

        if len(w) <= 2 {
            continue
        }

        //Debugging print
        //fmt.Printf("%s\n", w)
        percent := float64(words_scanned)/float64(word_count)*100.0
        fmt.Printf("Processing text file: %3d%% (%d/%d).\n", int(percent), words_scanned, word_count)

        //insert into db
        insert_args += fmt.Sprintf("('%s'),", w)
        if words_scanned % sql_buffer == 0 {
          //remove comma
          insert_args = insert_args[:len(insert_args)-1] + ";"
          //fmt.Println(insert_stmt + insert_args)
          istmt,err := db.Prepare(insert_stmt + insert_args)
          check(err); istmt.Exec()
          insert_args = ""
        }

    }
    if len(insert_args) != 0 {
      //remove comma
      insert_args = insert_args[:len(insert_args)-1] + ";"
      //fmt.Println(insert_stmt + insert_args)
      istmt,err := db.Prepare(insert_stmt + insert_args)
      check(err); istmt.Exec()
    }
}

func calculate_frequencies() {
    _,err = db.Exec("DROP TABLE words1;")
    check(err)

  _,err := db.Exec(`
    CREATE TABLE words1 AS
    SELECT orig, transl, COUNT(orig) AS freq
    FROM words
    GROUP BY orig
    ORDER BY freq DESC
  `)
  check(err)
}

func connect_to_db() {
    db,err = sql.Open("sqlite3", dbname)
    check(err)

    //Prepare original table;
    _,err := db.Exec(fmt.Sprintf("CREATE TABLE IF NOT EXISTS %s (id INTEGER PRIMARY KEY, orig TEXT, transl TEXT DEFAULT 'TODO', freq INTEGER DEFAULT 1)", tbname))
    check(err);

    fmt.Printf("Connected to database: %s, containing table: %s.\n", dbname, tbname)
}

func api_call(index int, concurrent bool) {
    if concurrent {
        defer func() {
            api_call_counter <- index
            }()
    }
    if index >= len(samples) {
        fmt.Printf("index: %d greater than len(samples): %d\n", index, len(samples))
        return
    }
    phrase := samples[index].orig
    src_lng := "de"
    dst_lng := "en"
    url := fmt.Sprintf("https://glosbe.com/gapi/translate?from=%s&dest=%s&format=json&phrase=%s", src_lng, dst_lng, phrase)
    resp,err := client.Get(url)
    check(err)
    defer resp.Body.Close()

    buff,err := ioutil.ReadAll(resp.Body)
    check(err)
    //fmt.Printf("%s\n", string(buff))
    obj := ApiResult{}
    json.Unmarshal(buff, &obj)
    //fmt.Println(obj.Result)
    //fmt.Println(obj)
    if (obj.Result == "ok" && len(obj.Tuc) > 0) {
        ret := obj.Tuc[0].Phrase.Text
        if ret != "" {
            samples[index].transl = ret
            return
        }
    } 
    samples[index].transl = TRANSL_ERR
}

func translate_samples() {
    fmt.Println("Translating samples.")
    api_call_counter = make(chan int)

    if output_cap > len(samples) {
        output_cap = len(samples) 
    }

    //start iterating over samples
    samples_idx := 0
    for samples_idx=0; samples_idx<output_cap; samples_idx++ {
        go api_call(samples_idx, true)
    }    
    //catch all goroutines
    nr_fails := 0
    for i:=0; i<output_cap; i++ {
        tmp := <- api_call_counter
        fmt.Printf("orig: %s, transl: %s\n", samples[i].orig, samples[i].transl)
        if samples[tmp].transl == TRANSL_ERR {
            nr_fails++
        }
    }

    //fix fails
    for samples_idx:=samples_idx; nr_fails > 0 && samples_idx < output_cap; samples_idx++ {
        api_call(samples_idx, false)
        if samples[samples_idx].transl != TRANSL_ERR {
            nr_fails--
        }
    }
    fmt.Println("Translation complete.")
}

func print_samples() {
    fmt.Println("Samples:")
    i:=1    //for printing
    for _,v := range samples {
        if v.transl == TRANSL_ERR || v.transl == "TODO" {
            continue
        }
        fmt.Printf("%3d.  %-20s %-20s %d\n", i, v.orig, v.transl, v.freq)
        i++
    } 
}

func extract_output_samples() {
    for _,v := range samples {
        if v.transl != "TODO" && v.transl != TRANSL_ERR {
            output_samples = append(output_samples, v)
        }
    } 
}

func main() {
    fmt.Println("Launching Dictionizer")

    filename = "also_sprach_zarathustra_full.txt"

    //process txt file name, prepare sqlite db filename
    name_arr := strings.Split(filename, ".")
    if len(name_arr) > 1 {
        filename = name_arr[0]
    }

    dbname = fmt.Sprintf("./tmp_db/%s.db", filename)
    output_filename = fmt.Sprintf("./output_pdf/%s.pdf", filename)

    connect_to_db()

    //read_file(filename)
    calculate_frequencies()
    set_rf_prob()
    sample_wrapper()
    print_samples()
    translate_samples()
    extract_output_samples()
    draw_pdf()

    db.Close()

}
