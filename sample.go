package main

import (
  "fmt"
  "math"
  "math/rand"
  "time"
)

//probability that words with frequency f will be sampled
//usage: words with a frequency freq has tf_prob[freq] chance of being sampled

func sample_wrapper() {
  //keep reducint sample_weight and calling sample until you're left with enough samples or sample_weight is 0
  for len(samples) < output_cap*2 && sampling_weight > 0 {
    samples = nil //legit way to release memory
    sample()
    sampling_weight -= 0.1
  }
}

func sample() {
  //set random seed
  rand.Seed(time.Now().Unix())

  res,err := db.Query("SELECT * FROM words1")
  check(err)
  for res.Next() {
    var w1s w1_struct
    res.Scan(&w1s.orig, &w1s.transl, &w1s.freq)
    rtmp := rand.Float64()
    if rtmp * sampling_weight < rf_prob[w1s.freq] {
      //fmt.Printf("random: %6f, freq: %4d, freq-prob: %6f, orig: %s\n", rtmp, w1s.freq, rf_prob[w1s.freq], w1s.orig)
      samples = append(samples, w1s)
      if (len(samples) >= output_cap*2) {    //half of the samples will be backup if translation api fails
        break
      }
    }
  }
  fmt.Printf("Sampled: %d samples. Requested: %d samples. (2*output_cap). sampling_weight: %0.1f\n", len(samples), output_cap*2, sampling_weight)
}

func set_rf_prob() {
  rf_prob = make(map[int]float64)

  res,err := db.Query("SELECT freq FROM words1 GROUP BY freq")
  check(err)
  var rfs []int
  for res.Next() {
    var (
      freq int
    )
    res.Scan(&freq)
    rfs = append(rfs, freq)
  }

  //fmt.Println(rfs)

  var probs []float64
  sum := 0.0
  for i := range(rfs) {
    g := gauss(i,20,0.1)
    sum += g
    probs = append(probs, g)
  }
  //test_sum := 0.0
  //normalize
  for i,p := range(probs) {
    rf_prob[rfs[i]] = p/sum
    //probs[i] = p/sum
    //test_sum += probs[i]
  }
  //fmt.Println(test_sum) //it's 1
}

func gauss(x int, mu int, sigma float64) float64 {
  return ( (1.0/math.Sqrt(2*math.Pi*math.Pow(sigma,2)) * math.Exp((-1)*(math.Pow(float64(x-mu),2)/2*math.Pow(sigma,2)))) )
}
